# explore VAERS


Requirements:
- Python >=3.8
- git-lfs

```
brew install git-lfs
git lfs install
git lfs pull
pip install -r requirements.txt
```

# Using Gitpod

You can launch a Gitpod workspace from this Gitlab project page.

![image.png](./docs/gitpod.png)

From a fresh Gitpod workspace, open the terminal and run `sh gitpod_init.sh` to install git-lfs, pull the large data files and install the python requirements.


