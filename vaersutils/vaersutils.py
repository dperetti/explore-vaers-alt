from typing import List, Optional
from math import ceil
from dataclasses import dataclass
import pandas as pd
from pandas import DataFrame
import seaborn as sns

NUM_DAYS_MIN = 0
NUM_DAYS_MAX = 28


@dataclass
class Injection:
    data: DataFrame
    label: str
    sql: Optional[str] = 'True'  # noop

    def table(self, age_min=12, age_max=100, num_days_min=None, num_days_max=None, randomize=0, frac=1):
        """
        Return a filtered dataframe of the data, by vaccine manufacturer / vaccine dose,
        age and number of days between injection and onset.
        Add an 'injection' column.
        """
        num_days_min = num_days_min if num_days_min is not None else NUM_DAYS_MIN
        num_days_max = num_days_max if num_days_max is not None else NUM_DAYS_MAX

        def from_query(sql):
            return pd.DataFrame(
                self.data.query(
                    f'{sql}'
                    f' and AGE_YRS >= {age_min} and AGE_YRS < {age_max}'
                    f' and NUMDAYS >= {num_days_min} and NUMDAYS <= {num_days_max}'
                )
            )

        d = from_query(self.sql)


        # Discard the original data and return a random sample of the whole dataset (all vaccines)
        # yet having the same characteristics (size and filters).
        if randomize > 0:
            # remember the size of the table
            n = len(d)
            # re-do the query with no vaccine filter
            d2 = from_query('True')  # all vaccines

            # If frac is passed, only replace a fraction of the original data with random data
            if frac < 1:
                import numpy
                indexes_choice = numpy.random.choice(n, int(n * frac), replace=False)
                for idx, idx_choice in enumerate(indexes_choice):
                    d.iloc[idx_choice] = d2.iloc[idx]
            else:
                # replace data with a sample
                d = d2.sample(n=n, random_state=randomize, replace=False, ignore_index=False)

        # add an 'injection' column
        d.loc[:, 'injection'] = self.label

        return d

    def num_days_stats_by_age(self, groups=3, **kwargs):
        """
        Return a list of Series [injection | age group | median of number of days to onset | count of events]
        """
        arr = []
        start = 12  # start at 12y
        span = 90 / groups  # ends at 102y
        # create the age groups
        for x in range(groups):
            age_min = start + span * x
            age_max = start + span * (x + 1)
            t = self.table(age_min=int(age_min), age_max=int(age_max), **kwargs)
            # generate the stats with DataFrame.describe()
            # We keep '50%' (= median) and 'count'.
            d = t['NUMDAYS'].describe()
            arr.append(pd.Series([self.label, f'{ceil(age_min)}-{ceil(age_max)}', d['50%'], d['count']],
                                 index=['injection', 'age', 'median', 'count']))
        return arr



def plot_hists(arr: List[Injection], randomize=0, **kwargs):
    """
    Plot a grid of histograms.
    Ex: hist([Moderna2, Janssen], bins=8, binrange=((0, 100), (0, 28)))
    """
    tables = [a.table(randomize=randomize) for a in arr]
    data_frame = pd.concat(tables)
    g = sns.FacetGrid(data=data_frame, col='injection')

    # Set smart defaults for the histogram's bins but allow override.
    kwargs['bins'] = kwargs.get('bins', (25, NUM_DAYS_MAX - NUM_DAYS_MIN))
    #if 'bins' in kwargs:
    #    del kwargs['bins']

    # Set thresh to None by default (instead of 0), unless specified
    kwargs['thresh'] = kwargs.get('thresh', None)
    kwargs['cbar'] = kwargs.get('cbar', True)

    g.map(sns.histplot, 'AGE_YRS', 'NUMDAYS', **kwargs)
    g.data_frame = data_frame
    for idx, a in enumerate(arr):
        injection = arr[idx]
        g.axes[0, idx].set_title(f"{injection.label} n={len(tables[idx])}{' [rand]' if randomize else ''}")
    return g


def plot_chart(arr: List[Injection], **kwargs):
    """
    Ex: plot([PFIZER1, PFIZER2], groups=3, num_days_min=1)
    """
    import itertools
    data_frame = pd.DataFrame(itertools.chain(*[injection.num_days_stats_by_age(**kwargs) for injection in arr]))
    p = sns.lineplot(data=data_frame, x='age', y='median', hue='injection')
    p.set(ylim=(0, 10))
    p.set(title=f"Median n. of days to death (VAERS){' [rand]' if kwargs.get('randomize', 0) > 0 else ''}")
    # Attach the data to the plot so we can easily inspect it
    p.data_frame = data_frame
    return p